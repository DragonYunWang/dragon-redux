export function createStore(reducer, enhancer) {
  // 对参数进行处理
  // if (typeof preloadedState === 'function' && typeof enhancer === 'undefined') {
  //   enhancer = preloadedState
  //   preloadedState = undefined
  // }
  // if (typeof enhancer !== 'undefined') {
  //   if (typeof enhancer !== 'function') {
  //     throw new Error('Expected the reducer to be a function.')
  //   }
  // }
  if (enhancer) {
    return enhancer(createStore)(reducer)
  }
  let currentReducer = reducer
  let currentState = {}
  let currentListeners = []

  function getState() {
    return currentState
  }

  function subscribe(listener) {
    currentListeners.push(listener)
  }

  function dispatch(action) {
    currentState = currentReducer(currentState, action)
    currentListeners.forEach(v => v())
    return action
  }

  dispatch({ type: '__@DRAGON_REDUX@__' })

  return { getState, subscribe, dispatch }
}

const bindActionCreator = (creator, dispatch) => {
  return (...args) => dispatch(creator(...args))
}

export const bindActionCreators = (creators, dispatch) => {
  const keys = Object.keys(creators)
  return keys.reduce((ret, item) => {
    ret[item] = bindActionCreator(creators[item], dispatch)
    return ret
  }, {})
}

// applyMiddleware 执行过后返回一个闭包函数，
// 目的是将创建 store的步骤放在这个闭包内执行，这样 middleware 就可以共享 store 对象

export const applyMiddleware = (...middwares) => {
  return createStore => (...args) => {
    const store = createStore(...args)
    let dispatch = store.dispatch
    let chain = []
    let middwareAPI = {
      getState: store.getState,
      dispatch: action => dispatch(action)
    }
    // middlewares 数组 map 为新的 middlewares 数组，包含了 middlewareAPI
    chain = middwares.map(middleware => middleware(middwareAPI))
    console.log('TCL: applyMiddleware -> chain', chain)
    // compose 方法将新的 middlewares 和 store.dispatch 结合起来，生成一个新的 dispatch 方法
    dispatch = compose(...chain)(store.dispatch)

    return {
      ...store,
      dispatch
    }
  }
}

function compose(...funcs) {
  if (funcs.length === 0) {
    return arg => arg
  }

  if (funcs.length === 1) {
    return funcs[0]
  }

  const last = funcs[funcs.length - 1]
  const rest = funcs.slice(0, -1)
  return (...args) => rest.reduceRight((composed, f) => f(composed), last(...args))
}
