import React from 'react'
import ReactDOM from 'react-dom'
import { Provider } from './clean_redux/dragon_react_redux'
import { store } from './clean_redux/store'
import App from './App'

ReactDOM.render(
  <Provider store={store}>
    <App />
  </Provider>,
  document.getElementById('root')
)

if (module.hot) {
  module.hot.accept()
}
