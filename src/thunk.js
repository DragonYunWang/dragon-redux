export const thunk = ({ dispatch, getState }) => next => action => {
  if (typeof action === 'function') {
    return action(dispatch, getState)
  }
  // 默认返回
  return next(action)
}

export const thunkArray = ({ dispatch, getState }) => next => action => {
  if (Array.isArray(action)) {
    return action.forEach(v => dispatch(v))
  }
  // 默认返回
  return next(action)
}