import { createStore, applyMiddleware } from '../dragon_redux'
import { thunk, thunkArray } from '../../thunk'
import reducer from './reducer'

const store = createStore(reducer, applyMiddleware(thunk, thunkArray))

export default store
