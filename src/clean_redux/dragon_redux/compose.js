export default function compose(...funcs) {
  if (funcs.length === 0) {
    return arg => arg
  }

  if (funcs.length === 1) {
    return funcs[0]
  }

  let last = funcs[funcs.length - 1]
  let rest = funcs.slice(0, -1)

  return (...args) => rest.reduceRight((composed, f) => f(composed), last(...args))
}
