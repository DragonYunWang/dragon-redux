import React, { Component } from 'react'
import PropTypes from 'prop-types'
import { bindActionCreators } from './dragon-redux'

export const connect = (mapStateToProps, mapDispatchToProps = {}) => WrapComponent => {
  return class extends Component {
    static contextTypes = {
      store: PropTypes.object
    }

    constructor(props, context) {
      super(props, context)
      this.state = {
        props: {}
      }
    }

    render() {
      return <WrapComponent {...this.state.props} />
    }
    update() {
      const { store } = this.context
      // 绑定数据到组件中
      const stateProps = mapStateToProps(store.getState())
      // 将方法绑定到组件中
      let dispatchProps = {}
      if (
        typeof mapDispatchToProps === 'function' ||
        typeof mapDispatchToProps === 'object' ||
        // eslint-disable-next-line
        typeof mapDispatchToProps == 'undefind'
      ) {
        if (typeof mapDispatchToProps === 'function') {
          dispatchProps = mapDispatchToProps(store.dispatch)
        } else if (typeof mapDispatchToProps === 'object') {
          dispatchProps = bindActionCreators(mapDispatchToProps, store.dispatch)
        }
      } else {
        throw new Error('参数错误')
      }

      // 绑定
      this.setState({
        props: {
          ...this.state.props,
          ...stateProps,
          ...dispatchProps
        }
      })
    }
    componentDidMount() {
      const { store } = this.context
      store.subscribe(() => this.update())
      this.update()
    }
  }
}

export class Provider extends Component {
  static childContextTypes = {
    store: PropTypes.object
  }

  constructor(props, context) {
    super(props, context)
    this.store = props.store
  }

  getChildContext() {
    return { store: this.store }
  }

  render() {
    return this.props.children
  }
}
