import React, { Component } from 'react'
import { connect } from './clean_redux/dragon_react_redux'
import { actionCreators } from './clean_redux/store'
import { Button } from 'antd'

class App extends Component {
  render() {
    const { add, remove, addTwo, async } = this.props
    console.log('TCL: App -> render -> this.props', this.props)

    return (
      <div style={{ margin: '100px 100px' }}>
        count: {this.props.count}
        <br />
        <Button type="primary" onClick={add}>
          ADD
        </Button>
        <Button type="primary" onClick={remove}>
          REMOVE
        </Button>
        <Button type="primary" onClick={async}>
          ASYNC
        </Button>
        <Button type="primary" onClick={addTwo}>
          ADDTWO
        </Button>
      </div>
    )
  }
}

const mapStateToProps = state => ({
  count: state
})

const mapDispatchToProps = dispatch => ({
  add() {
    console.log(actionCreators.add())
    dispatch(actionCreators.add())
  },
  remove() {
    dispatch(actionCreators.remove())
  },
  async() {
    dispatch(actionCreators.async())
  },
  addTwo() {
    dispatch(actionCreators.addTwo())
  }
})

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(App)
