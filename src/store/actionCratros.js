import { ADD, REMOVE } from './actionTypes'

export const add = () => ({ type: ADD, count: 1 })

export const remove = () => ({ type: REMOVE, count: 1 })

export const async = () => dispatch => {
  setTimeout(() => {
    dispatch(add())
  }, 2000)
}
export const addTwo = () => [add(), async()]
