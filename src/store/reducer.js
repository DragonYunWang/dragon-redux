import * as types from './actionTypes'

const defaultState = 1314

export default function reducer(state, action) {
  switch (action.type) {
    case types.ADD:
      return state + action.count
    case types.REMOVE:
      return state - action.count
    default:
      return defaultState
  }
}
